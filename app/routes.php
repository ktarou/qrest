<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::group(array('prefix'=>'api/v1'), function()
{
	Route::get('surat', function()
	{
		return "All Surat";
	});
	
	Route::get('surat/loc/{loc}', function($loc)
	{
		return "All Surat using locale #$loc";
	});
	
	Route::get('surat/{surat}', function($surat)
	{
		return "Surat #$surat";
	});
	
	Route::get('surat/{surat}/loc/{loc}', function($surat, $loc)
	{
		return "Surat #$surat using locale #$loc";
	});
	
	Route::get('surat/{surat}/ayat', function($surat)
	{
		return "Surat #$surat with all ayat";
	});
	
	Route::get('surat/{surat}/ayat/loc/{loc}', function($surat, $loc)
	{
		return "Surat #$surat with all ayat using locale #$loc";
	});
	
	Route::get('surat/{surat}/ayat/{ayat}', function($surat, $ayat)
	{
		return "Surat #$surat with ayat #$ayat";
	});
	
	Route::get('surat/{surat}/ayat/{ayat}/loc/{loc}', function($surat, $ayat, $loc)
	{
		return "Surat #$surat with ayat #$ayat using locale #$loc";
	});
});